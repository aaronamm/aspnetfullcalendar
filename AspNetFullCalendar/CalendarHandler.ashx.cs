﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AspNetFullCalendar.Models;
using AspNetFullCalendar.Services;
using AspNetFullCalendar.lib;
using Newtonsoft.Json;
using log4net;

namespace AspNetFullCalendar
{
    /// <summary>
    /// Summary description for CalendarHandler
    /// </summary>
    public class CalendarHandler : IHttpHandler
    {
        private ILog _log = LogManager.GetLogger("web");

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "application/json";
            var request = context.Request;

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new DateTimeConverter("yyyy-MM-ddTHH:mm:ss"));
            switch (request.HttpMethod.ToUpper())
            {
                case "GET":


                    var today = DateTime.Today;
                    var year = today.Year;
                    var month = today.Month;
                    var day = today.Day;
                    var events = new List<CalendarEvent>()
                {
                    new CalendarEvent()
                    {
                    id =1,
                    start = new DateTime(year,month,day,8,0,0),

                    end = new DateTime(year,month,day,9,0,0),
                    title = "preventive"
                    }
                };


                    var jsonString = JsonConvert.SerializeObject(events, Formatting.Indented, settings);

                    context.Response.Write(jsonString);
                    _log.Debug("data sent back to client");
                    break;

                case "POST":
                    try
                    {
                        var body = request.InputStream;
                        var encoding = request.ContentEncoding;
                        var reader = new System.IO.StreamReader(body, encoding);
                        var requestJson = reader.ReadToEnd();
                        var e = JsonConvert.DeserializeObject<CalendarEvent>(requestJson);
                        _log.DebugFormat("title: {0}", e.title);
                        var calendarService = new CalendarService();
                        calendarService.AddEvent(e);

                        _log.Debug("data post back to client");

                        var json = JsonConvert.SerializeObject(new {status = true}, Formatting.Indented, settings);
                        context.Response.Write(json);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex);
                    }
                    break;

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}