﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using AspNetFullCalendar.Models;
using log4net;

namespace AspNetFullCalendar.Services
{
    public class CalendarService
    {
        private ILog _log = LogManager.GetLogger("web");

        public void AddEvent(CalendarEvent calendarEvent)
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["CalendarDb"].ConnectionString;
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var command = connection.CreateCommand())
                    {

                        var sql = string.Format(
                            @"INSERT dbo.CalendarEvent
        ( Title ,
          StartDate ,
          EndDate ,
          IsAllDay
        )
        VALUES  
          ( '{0}' , 
          '2014-03-02 10:00:00' , 
          '2014-03-02 12:00:00', 
           0  
        )", calendarEvent.title);

                        connection.Open();

                        command.CommandText = sql;
                        int rowEffect = command.ExecuteNonQuery();
                        _log.InfoFormat("rowEffect: {0}", rowEffect);
                    }
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }


        }



    }
}
