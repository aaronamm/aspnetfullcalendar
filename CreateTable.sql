USE CalendarDb
GO

CREATE TABLE [dbo].[CalendarEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](1000) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsAllDay] [bit] NOT NULL,
 CONSTRAINT [PK_CalendarEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_CalendarEvent_IsAllDay]    Script Date: 03/02/2014 18:53:12 ******/
ALTER TABLE [dbo].[CalendarEvent] ADD  CONSTRAINT [DF_CalendarEvent_IsAllDay]  DEFAULT ((0)) FOR [IsAllDay]
GO


